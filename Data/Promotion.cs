﻿namespace PRPTcode.Data
{
    public class Promotion
    {
        public int? Id { get; set; }
        public string Name { get; set; } = "";
        public decimal Discount { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

    }
}
