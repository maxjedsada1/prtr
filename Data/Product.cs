﻿namespace PRPTcode.Data
{
    public class Product
    {
        public int Id { get; set; } 
        public bool IsSale { get; set; } = false;
        public string Name { get; set; } 
        public string Detail { get; set; } 
        public string Picture { get; set; } 
        public decimal ScoreItem { get; set; } 
        public decimal PriceStarts  { get; set; } 
        public decimal PriceDiscount { get; set; } 
        public ProductType? ProductType { get; set; } = new ProductType();
        public Promotion? Promotion { get; set; } = new Promotion();

    }
}
