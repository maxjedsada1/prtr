﻿using Microsoft.EntityFrameworkCore;

namespace PRPTcode.Data
{
    public class Context : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "PRTRDb");
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Promotion> Promotions { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }

    }

}
