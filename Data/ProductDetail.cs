﻿namespace PRPTcode.Data
{
    public class ProductDetail
    {
        public int Id { get; set; } 
        public string Name { get; set; } 
        public string Detail { get; set; } 
        public string Picture { get; set; } 
        public string ProductTypeName { get; set; } 
        public bool IsAvailability { get; set; } = true;
        public string SKUText { get; set; }
        public decimal PriceStarts { get; set; }
        public decimal PriceDiscount { get; set; }

    }
}
