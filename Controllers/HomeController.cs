using Microsoft.AspNetCore.Mvc;
using PRPTcode.Data;
using PRPTcode.Models;
using PRPTcode.Repo.Interface;
using PRTR.Utility;
using System.Diagnostics;

namespace PRPTcode.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IProductRepository _productRepository;

        public HomeController(ILogger<HomeController> logger, IProductRepository productRepository)
        {
            _logger = logger;
            _productRepository = productRepository;
        }

        public IActionResult Index()
        {
            var data = _productRepository.GetProduct();
            if (data.Any())
            {
                // for API return 
                var responseAPI = ResponseWrapper.Success(System.Net.HttpStatusCode.Created, data);

                // for view (mvc) return
                return View(data);
            }

            return View();
        }

        public IActionResult GetProductById(int id)
        {
            var data = _productRepository.GetProductById(id);
            if (data != null)
            {
                return Json(data);
            }

            return Json("");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
