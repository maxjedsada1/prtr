﻿using System.Net;

namespace PRTR.Provider.Exception
{
    public class ProductException : BaseCustomException
    {
        public ProductException(string message, HttpStatusCode httpStatusCode) : base(message, "product", httpStatusCode) { }

        public class DataAlready : ProductException
        {
            public DataAlready(string id) : base($"product Id ({id}) is already exists.", HttpStatusCode.MultiStatus) { }
        }

    }
}
