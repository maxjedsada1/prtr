﻿using PRPTcode.Data;

namespace PRPTcode.Repo.Interface
{
    public interface IProductRepository
    {
        public List<Product> GetProduct();
        public ProductDetail GetProductById(int id);

    }
}
