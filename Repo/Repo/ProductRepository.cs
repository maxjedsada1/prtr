﻿using Microsoft.EntityFrameworkCore;
using PRPTcode.Data;
using PRPTcode.Repo.Interface;
using PRTR.Provider.Exception;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PRPTcode.Repo.Repo
{
    public class ProductRepository : IProductRepository
    {
        public ProductRepository(IWebHostEnvironment env)
        {
            var baseUploadDirectory = Path.Combine(env.ContentRootPath, "wwwroot");

            using (var context = new Context())
            {
                // can find dynamic by name path from database master ProductType

                DirectoryInfo diLighting = new DirectoryInfo(baseUploadDirectory + "/picProduct/lighting");
                DirectoryInfo diChair = new DirectoryInfo(baseUploadDirectory + "/picProduct/chair");
                FileInfo[] filesLighting = diLighting.GetFiles();
                FileInfo[] filesChair = diChair.GetFiles();

                var listPathPicLighting = filesLighting.Select(a => a.FullName).ToList();
                var listPathPicChair = filesChair.Select(a => a.FullName).ToList();

                Random rnd = new Random();

                var product = new List<Product>();
                product.Add(new Product
                {
                    Id = 1,
                    Name = "lamp SSS",
                    Detail = " bla bla bla",
                    ScoreItem = 2,
                    Picture = listPathPicLighting[rnd.Next(listPathPicLighting.Count)].Replace(baseUploadDirectory, "").Replace("\\", "/") ?? "/picProduct/brook.jpg",
                    ProductType = new ProductType
                    {
                        Name = "lighting"
                    },
                    Promotion = new Promotion
                    {
                        Name = "discout 10 bath",
                        StartDate = DateTime.Now.AddDays(-2),
                        EndDate = DateTime.Now.AddDays(2),
                    },
                    PriceStarts = 100,
                    PriceDiscount = 10,
                });
                product.Add(new Product
                {
                    Id = 2,
                    Name = "chair AAA",
                    Detail = " bla bla bla",
                    ScoreItem = 1.5m,
                    Picture = listPathPicChair[rnd.Next(listPathPicChair.Count)].Replace(baseUploadDirectory, "").Replace("\\", "/") ?? "/picProduct/brook.jpg",
                    ProductType = new ProductType
                    {
                        Name = "chair"
                    },
                    Promotion = new Promotion
                    {
                        Name = "discout 20 bath",
                        StartDate = DateTime.Now.AddDays(-2),
                        EndDate = DateTime.Now.AddDays(2),
                    },
                    PriceStarts = 100,
                    PriceDiscount = 20,
                });
                product.Add(new Product
                {
                    Id = 3,
                    Name = "chair lamp",
                    Detail = " bla bla bla",
                    ScoreItem = 3,
                    Picture = listPathPicChair[rnd.Next(listPathPicChair.Count)].Replace(baseUploadDirectory, "").Replace("\\", "/") ?? "/picProduct/brook.jpg",
                    ProductType = new ProductType
                    {
                        Name = "chair"
                    },
                    Promotion = new Promotion
                    {
                        Name = "discout 30 bath",
                        StartDate = DateTime.Now.AddDays(-2),
                        EndDate = DateTime.Now.AddDays(2),
                    },
                    PriceStarts = 100,
                    PriceDiscount = 30,
                });
                product.Add(new Product
                {
                    Id = 4,
                    Name = "lamp",
                    Detail = " bla bla bla",
                    ScoreItem = 2.5m,
                    Picture = listPathPicLighting[rnd.Next(listPathPicLighting.Count)].Replace(baseUploadDirectory, "").Replace("\\", "/") ?? "/picProduct/brook.jpg",
                    ProductType = new ProductType
                    {
                        Name = "lighting"
                    },                    
                    PriceStarts = 100,
                    PriceDiscount = 0,
                });
                product.Add(new Product
                {
                    Id = 5,
                    Name = "chair SSS",
                    Detail = " bla bla bla",
                    ScoreItem = 5,
                    Picture = listPathPicChair[rnd.Next(listPathPicChair.Count)].Replace(baseUploadDirectory, "").Replace("\\", "/") ?? "/picProduct/brook.jpg",
                    ProductType = new ProductType
                    {
                        Name = "chair"
                    },
                    Promotion = new Promotion
                    {
                        Name = "discout 40 bath",
                        StartDate = DateTime.Now.AddDays(-2),
                        EndDate = DateTime.Now.AddDays(2),
                    },
                    PriceStarts = 100,
                    PriceDiscount = 40,
                });

                try
                {
                    context.Products.AddRange(product);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    //throw new ProductException.DataAlready(string.Join(", ", product.Select(a => a.Id)));
                }

            }

        }

        public List<Product> GetProduct()
        {
            using (var context = new Context())
            {
                var list = context.Products
                            .Include(a => a.ProductType)
                            .Include(a => a.Promotion)
                            .ToList();
                return list;
            }
        }

        public ProductDetail GetProductById(int id)
        {
            using (var context = new Context())
            {
                var data = (from product in context.Products
                            where product.Id == id
                            select new ProductDetail
                            {
                                Id = product.Id,
                                Name = product.Name,
                                Detail = product.Detail,
                                Picture = product.Picture,
                                ProductTypeName = product.ProductType.Name,
                                IsAvailability = product != null ? true : false,
                                SKUText = context.Products.Where(a => a.Id == product.Id).Count().ToString() ?? "N/A",
                                PriceStarts = product.PriceStarts,
                                PriceDiscount= product.PriceDiscount,
                            })
                            .FirstOrDefault();

                if (data == null)
                {
                    return new ProductDetail();
                }

                return data;
            }

        }

    }
}
