﻿$(document).ready(function () {
    console.log("ready");

});

function GetProductById(id) {

    $.get("Home/GetProductById", { id: id })
        .done(function (data) {


            $('#myModal').modal('toggle');
            $('#picProduct').attr('src', data.picture);
            $('#id').text(data.id);
            $('#name').text(data.name);
            $('#detail').text("Detail : " + data.detail);
            $('#productTypeName').text("Type : " +data.productTypeName);

            var stringAvailability = data.isAvailability == true ? '<p style="color: green;"> Availability : In stock </p>'
                : '<p style="color: red;">Availability : Sold out</p>';

            $('#isAvailability').append().empty();
            $('#isAvailability').append(stringAvailability);

            $('#skuText').text(data.sKUText);

            $('#priceStarts').text(data.priceStarts);
            $('#priceDiscount').text(data.priceDiscount);

            var priceAll = data.priceStarts - data.priceDiscount;
            var stringPrice = data.priceDiscount <= 0
                ? '<p style="color: red; display:inline;"> $' + data.priceStarts.toString() + ' </p>'
                : '<p style="text-decoration: line-through; display:inline;"> $' + data.priceStarts.toString() + ' </p> <p style="color: red; display:inline;"> $' + priceAll.toString() + ' </p > '

            $("#priceAll").append().empty();
            $("#priceAll").append(stringPrice);

        });
}

function CloseModal() {
    $('#myModal').modal('toggle');
}

